<?php

require 'FBAuth.class.php';

$FBAuth = new myFacebook();

if(isset($_SESSION['fb_access_token'])){
  echo "Access Token Already Stored <br>";
  echo $_SESSION['fb_access_token'];
  echo "<br>";
}else {

  $permissions = ['email'];
  $loginUrl = $FBAuth->getLoginUrl($permissions);
  echo '<a href="' . $loginUrl . '">Log in with Facebook!</a>';

  $accessToken = $FBAuth->getAccessToken();
  
  if(strpos($FBAuth->checkIfError($accessToken), 'Error') !== 0){
    if(strpos($FBAuth->getAccessTokenValue($accessToken), 'Error') !== 0){

      echo '<h3>Access Token</h3>';
      echo $FBAuth->getAccessTokenValue($accessToken);
      // The OAuth 2.0 client handler helps us manage access tokens
      $FBAuth->fb->getOAuth2Client();
      echo "<br><br>";
      // Get the access token metadata from /debug_token
      echo "<h3>Metadata</h3> <br>";
      echo '<pre>';
      echo var_dump($FBAuth->fb->getOAuth2Client()->debugToken($accessToken));
      echo '</pre>';

      // Validation (these will throw FacebookSDKException's when they fail)
      $FBAuth->validateAppId($accessToken);
      
      $FBAuth->validateExpiration($accessToken);

      echo "<br><br>" . $FBAuth->ifTokenIsLongLived($accessToken);

      $FBAuth->storeAccessTokenInSession($accessToken);
      
      echo "<br><br>";
      echo "user name : " . $FBAuth->getUserName($accessToken);
      echo "<br>";
      echo "user id : " . $FBAuth->getUserId($accessToken);

    }else {
      echo "<br><br>" . $FBAuth->getAccessTokenValue($accessToken) . "<br><br>";
    }
  }else {
    echo "<br><br>" . $FBAuth->checkIfError($accessToken) . "<br><br>";
  }

  
}

?>