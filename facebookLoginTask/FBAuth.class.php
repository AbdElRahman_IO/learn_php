<?php

require 'vendor/autoload.php';

class myFacebook {

    public $fb;
    private $conf = [
        'app_id' => '416127638884079',
        'app_secret' => '2fcf6a08bf44bf18dee818639e13d583',
        'default_graph_version' => 'v2.2',
    ];
    private $accessToken;
    
    function __construct() {
        $this->startSession();
        $this->fb = new Facebook\Facebook($this->conf);
    }

    function startSession() {
        if (!session_id()) {
            session_start();
        }
    }

    function validateAppId($accessToken) {
        $this->fb->getOAuth2Client()->debugToken($accessToken)->validateAppId($this->conf['app_id']);
    }

    function getLoginUrl($permissions) {
        $loginUrl = $this->fb->getRedirectLoginHelper()->getLoginUrl('http://localhost/learnPhp/facebookLoginTask/', $permissions);
        return $loginUrl;
    }

    function getAccessToken() {
        try {
            $accessToken = $this->fb->getRedirectLoginHelper()->getAccessToken();
            $this->$accessToken = $accessToken;
            return $accessToken;
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            return 'Error - Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            return 'Error - Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
    }

    function checkIfError($accessToken) {
        echo "test" . $this->accessToken;
        if (! isset($accessToken)) {
            if ($this->fb->getRedirectLoginHelper()->getError()) {
                header('HTTP/1.0 401 Unauthorized');
                return "Error : " . $this->fb->getRedirectLoginHelper()->getError() . "\n";
                return "Error Code: " . $this->fb->getRedirectLoginHelper()->getErrorCode() . "\n";
                return "Error Reason: " . $this->fb->getRedirectLoginHelper()->getErrorReason() . "\n";
                return "Error Description: " . $this->fb->getRedirectLoginHelper()->getErrorDescription() . "\n";
            } else {
                header('HTTP/1.0 400 Bad Request');
                return 'Error Bad request';
            }
            exit;
        }
    }

    function getAccessTokenValue($accessToken) {
        if(!is_null($accessToken)){
            return $accessToken;
        }
    }

    function validateExpiration($accessToken) {
        return $this->fb->getOAuth2Client()->debugToken($accessToken);
    }

    function storeAccessTokenInSession($accessToken) {
        $_SESSION['fb_access_token'] = (string) $accessToken;
    }

    function validateUserId($userId, $accessToken) {
        return $this->fb->getOAuth2Client()->debugToken($accessToken)->validateUserId($userId);
    }

    function ifTokenIsLongLived($accessToken) {
        if (! $accessToken->isLongLived()) {
            // Exchanges a short-lived access token for a long-lived one
            try {
              $accessToken = $this->fb->getOAuth2Client()->getLongLivedAccessToken($accessToken);
              return $accessToken;
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
              echo "Error getting long-lived access token: " . $e->getMessage();
              return $accessToken;
            }
            return '<h3>Long-lived</h3> <br> <pre>' . var_dump($accessToken->getValue()) . '</pre>';
          }
    }

    function getUserData($accessToken) {
        try {
            // Get the \Facebook\GraphNodes\GraphUser object for the current user.
            // If you provided a 'default_access_token', the '{access-token}' is optional.
            $response = $this->fb->get('/me', $accessToken);
          } catch(\Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            return 'Graph returned an error: ' . $e->getMessage();
            exit;
          } catch(\Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            return 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
          }
    
          $me = $response->getGraphUser();
          return $me;
        //   return 'Logged in as ' . $me->getName() . '<br> <pre>' . var_dump($me) . '</pre>';
    }

    function getUserName($accessToken) {
        return $this->getUserData($accessToken)->getName();
    }

    function getUserId($accessToken) {
        return $this->getUserData($accessToken)->getId();
    }

}

?>