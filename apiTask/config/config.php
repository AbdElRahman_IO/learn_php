<?php

  class Connection {
    
    private $host = "localhost";
    private $dbName = "testapi";
    private $username = "root";
    private $password = "";
    public $pdo;
    
    function __construct() {
        $this->dbConnect();
    }
    
    private function dbConnect()
    {
        try {
            echo "[-] Start Connecting .. <br>";
            $this->pdo = new \PDO('mysql:dbname='. $this->dbName .';host='. $this->host, $this->username, $this->password);
            $this->pdo->query("SET NAMES utf8;");
            $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $this->pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ);
            echo "[-] Connection Success .. <br>"; 
        }
        catch(PDOException $e) {
            $this->pdo = "err";
            echo "[-] Connection Failed .. <br>";
            echo "[-] " . $e->getMessage();
        }
    }
    
  }

?>