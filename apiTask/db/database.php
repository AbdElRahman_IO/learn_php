<?php

require('config/config.php');

class Database {

    private $postsTable = "
    CREATE TABLE IF NOT EXISTS `page` (
        `id` INT AUTO_INCREMENT NOT NULL,
        `title` varchar(200),
        `content` TEXT,
        `category_id` INT
      ) CHARACTER SET utf8 COLLATE utf8_general_ci;
      ";

    private $catsTable = "
    CREATE TABLE IF NOT EXISTS `categories` (
        `id` INT AUTO_INCREMENT NOT NULL,
        `name` varchar(200)
    ) CHARACTER SET utf8 COLLATE utf8_general_ci;
    ";

    function __construct(){
        // $connect = new Connection();
        $this->createDb();
    }

    private function createDb()
    {
        $stmt = $this->connect->pdo->prepare($this->postsTable);
        $stmt->execute();
        $stmt = $this->connect->pdo->prepare($this->catsTable);
        $stmt->execute();
    }

}


/*
// to set data
$query = 'INSERT INTO posts (title, discretion, image, tags)
VALUES ("title test 3", "discretion test 3", "image test 3", "image tags 3")';
$stmt = $pdo->prepare($query);
$stmt->execute();
*/


// to set data
// $query = 'INSERT INTO categories (name)
// VALUES ("category title test 2")';
// $stmt = $connect->pdo->prepare($query);
// $stmt->execute();



?>